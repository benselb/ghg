package hex

import (
	"fmt"
	"io"
	"os"
)

func die(e error) {
	fmt.Fprintf(os.Stderr, "%s\n", e)
	os.Exit(1)
}

func printLine(address int64, line []byte) {
	fmt.Printf("%08x: %- 48x ", address, line)
	for i, c := range line {
		if c < ' ' || c > '~' {
			line[i] = '.'
		}
	}
	fmt.Printf("%s\n", string(line))
}

func dump(f *os.File) {
	buffer := make([]byte, 10*1024*1024)
	extra := 0
	totalWritten := int64(0)
	for {
		count, err := f.Read(buffer[extra:])
		if err == io.EOF {
			break
		} else if err != nil {
			die(err)
		}
		count += extra
		extra = 0

		for i := 0; i < count-15; i += 16 {
			printLine(totalWritten, buffer[i:i+16])
			totalWritten += 16
		}
		extra = count % 16
		copy(buffer[:extra], buffer[count-extra:])
	}

	if extra != 0 {
		printLine(totalWritten, buffer[:extra])
	}
}

func Main() {
	if len(os.Args) > 1 {
		for _, arg := range os.Args[1:] {
			f, err := os.Open(arg)
			if err != nil {
				die(err)
			}
			dump(f)
		}
	} else {
		dump(os.Stdin)
	}
}
