package tree

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"sort"
	"strings"

	"github.com/tredoe/term"
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: ghg tree [-a] [directory]\n")
	os.Exit(1)
}

func die(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(2)
	}
}

func loadColors() (colors map[string]string) {
	colors = make(map[string]string)
	if !term.IsTerminal(1) {
		return
	}
	envVar := os.Getenv("LS_COLORS")
	types := strings.Split(envVar, ":")
	for _, t := range types {
		split := strings.SplitN(t, "=", 2)
		if len(split) != 2 {
			continue
		}
		colors[split[0]] = split[1]
	}
	return
}

type ByName []os.FileInfo

func (fi ByName) Len() int {
	return len(fi)
}
func (fi ByName) Swap(i, j int) {
	fi[i], fi[j] = fi[j], fi[i]
}
func (fi ByName) Less(i, j int) bool {
	return fi[i].Name() < fi[j].Name()
}

func tree(dir string, all bool, indent int, depth int, colors map[string]string) bool {
	if depth == 0 {
		return true
	}

	spacing := strings.Repeat(" ", indent)

	d, err := os.Open(dir)
	if err != nil {
		fmt.Printf("%serror: %s\n", spacing, err)
		return false
	}

	files, err := d.Readdir(0)
	d.Close()
	if err != nil {
		fmt.Printf("%serror: %s\n", spacing, err)
		return false
	}

	sort.Sort(ByName(files))
	for _, f := range files {
		if all || f.Name()[0] != '.' {
			indicator := ""
			recur := false
			color := colors["fi"]
			switch {
			case (f.Mode() & os.ModeDir) != 0:
				color = colors["di"]
				indicator = "/"
				if f.Name() != "." && f.Name() != ".." {
					recur = true
				}
			case (f.Mode() & os.ModeSymlink) != 0:
				color = colors["ln"]
				if _, err := os.Stat(dir + "/" + f.Name()); err != nil {
					color = colors["or"]
				}
				indicator = "@"
			case (f.Mode() & os.ModeSocket) != 0:
				color = colors["so"]
				indicator = "="
			case (f.Mode() & os.ModeNamedPipe) != 0:
				color = colors["pi"]
				indicator = "|"
			case (f.Mode() & 0111) != 0:
				color = colors["ex"]
				indicator = "*"
			}

			if color == "" {
				fmt.Printf("%s%s%s\n", spacing, f.Name(), indicator)
			} else {
				fmt.Printf("%s\033[%sm%s\033[0m%s\n", spacing, color, f.Name(), indicator)
			}

			if recur {
				tree(dir+"/"+f.Name(), all, indent+2, depth-1, colors)
			}
		}
	}

	return true
}

func Main() {
	all := false
	depth := 1000
	less := false

	flag.BoolVar(&all, "a", false, "include hidden files")
	flag.IntVar(&depth, "depth", 1000, "maximum depth (<0 for infinite)")
	flag.BoolVar(&less, "less", true, "pipe colored output to less -RFX")
	flag.Parse()

	dirs := flag.Args()
	if len(dirs) == 0 {
		dirs = []string{"."}
	}

	_, pathErr := exec.LookPath("less")
	if less && pathErr == nil {
		r, w, err := os.Pipe()
		die(err)

		cmd := exec.Command("less", "-RFX")
		cmd.Stdin = r
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		die(cmd.Start())

		r.Close()
		os.Stdout = w

		defer cmd.Wait()
		defer w.Close()
	}

	for _, dir := range dirs {
		if !tree(dir, all, 0, depth, loadColors()) {
			os.Exit(2)
		}
	}
}
