What?
-----

```
$ ghg
ghg is a multi-call binary with these commands:
 hex       Creates a hexdump of a file.
 pv        Watches the progress of a pipeline.
 rand      Generates (secure) random numbers and passwords.
 rpn       A basic RPN calculator.
 throttle  Slows a pipeline to a specified rate.
 tree      Lists the contents of a directory recursively.
```

Download
--------

Pre-compiled executables can be [downloaded from Bintray](https://bintray.com/selb/go/ghg).
