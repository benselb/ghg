package main

import (
	"fmt"
	"os"
	"path/filepath"

	"bitbucket.org/benselb/ghg/hex"
	"bitbucket.org/benselb/ghg/pv"
	"bitbucket.org/benselb/ghg/rand"
	"bitbucket.org/benselb/ghg/rpn"
	"bitbucket.org/benselb/ghg/throttle"
	"bitbucket.org/benselb/ghg/tree"
)

type command struct {
	name string
	doc  string
	main func()
}

var commands = []command{
	{"hex", "Creates a hexdump of a file.", hex.Main},
	{"pv", "Watches the progress of a pipeline.", pv.Main},
	{"rand", "Generates (secure) random numbers and passwords.", rand.Main},
	{"rpn", "A basic RPN calculator.", rpn.Main},
	{"throttle", "Slows a pipeline to a specified rate.", throttle.Main},
	{"tree", "Lists the contents of a directory recursively.", tree.Main},
}

func help() {
	fmt.Fprintf(os.Stderr, "ghg is a multi-call binary with these commands:\n")
	for _, c := range commands {
		fmt.Fprintf(os.Stderr, " %-8.8s  %s\n", c.name, c.doc)
	}
}

func dispatch() {
	if len(os.Args) == 0 {
		help()
		return
	}

	cmd := filepath.Base(os.Args[0])

	if cmd == "ghg" {
		os.Args = os.Args[1:]
		dispatch()
		return
	}

	for _, c := range commands {
		if cmd == c.name {
			if c.main != nil {
				c.main()
			}
			return
		}
	}

	fmt.Fprintf(os.Stderr, "ghg %s?\n", cmd)
}

func main() {
	dispatch()
}
