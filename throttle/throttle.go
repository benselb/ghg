package throttle

import (
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"time"
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: ghg throttle rate\n\nrate is in bytes/second and may have a suffix of K, M, or G.\n")
	os.Exit(2)
}

func Main() {
	args := os.Args[1:]
	if len(args) != 1 {
		usage()
	}

	rateString := args[0]
	multiplier := float64(1)
	if len(rateString) > 0 {
		switch rateString[len(rateString)-1] {
		case 'K':
			multiplier = 1024
		case 'M':
			multiplier = 1024 * 1024
		case 'G':
			multiplier = 1024 * 1024 * 1024
		}

		if multiplier > 1 {
			rateString = rateString[0 : len(rateString)-1]
		}
	}

	rate, err := strconv.ParseFloat(rateString, 64)
	if err != nil {
		usage()
	}
	rate *= multiplier

	ticker := time.Tick(time.Second / time.Duration(128))

	prevT := time.Now()
	partial := float64(0)
	for t := range ticker {
		dt := t.Sub(prevT)
		prevT = t
		if dt <= 0 {
			continue
		}

		var n float64
		n, partial = math.Modf(dt.Seconds()*rate + partial)

		if n != 0 {
			_, err := io.CopyN(os.Stdout, os.Stdin, int64(n))
			if err == io.EOF {
				return
			} else if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}
}
