package pv

import (
	"fmt"
	"io"
	"os"
	"time"
)

func display(text string) {
	fmt.Fprintf(os.Stderr, "\x1B[1G\x1B[2K%s", text)
}

func formatBytes(n int64) string {
	var unit string = ""
	var scaled float64 = 0

	switch {
	case n < 1024:
		unit = "B "
		scaled = float64(n)
	case n < 1024*1024:
		scaled = float64(n) / 1024
		unit = "KB"
	case n < 1024*1024*1024:
		scaled = float64(n) / (1024 * 1024)
		unit = "MB"
	default:
		scaled = float64(n) / (1024 * 1024 * 1024)
		unit = "GB"
	}

	return fmt.Sprintf("% 6.01f %s", scaled, unit)
}

func Main() {
	var nwritten int64 = 0
	var blockSize int64 = 1

	startTime := time.Now()

	display(formatBytes(0))
	for {
		n, err := io.CopyN(os.Stdout, os.Stdin, blockSize)

		if err != nil {
			fmt.Fprintf(os.Stderr, "\n")
			if err != io.EOF {
				fmt.Fprintf(os.Stderr, "%s\n", err)
			}
			return
		}

		nwritten += n
		dt := int64(time.Since(startTime).Seconds())
		if dt == 0 {
			dt = 1
		}
		speed := nwritten / dt
		display(formatBytes(nwritten) + "  (" + formatBytes(speed) + "/s)")

		if speed > 4 {
			blockSize = speed / 4
			if blockSize > 8*1024*1024 {
				blockSize = 8 * 1024 * 1024
			}
		}
	}
}
