package rpn

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

var stack []float64 = make([]float64, 0, 8)

func pop() (top float64) {
	if len(stack) == 0 {
		fmt.Fprintf(os.Stderr, "Stack underflow\n")
		return
	}

	top = stack[len(stack)-1]
	stack = stack[:len(stack)-1]

	return
}

func push(v float64) {
	stack = append(stack, v)
}

type command struct {
	name string
	does func()
}

var commands = []command{
	{"+", func() { b := pop(); a := pop(); push(a + b) }},
	{"-", func() { b := pop(); a := pop(); push(a - b) }},
	{"*", func() { b := pop(); a := pop(); push(a * b) }},
	{"/", func() { b := pop(); a := pop(); push(a / b) }},
	{"%", func() { b := pop(); a := pop(); push(math.Mod(a, b)) }},
	{"**", func() { b := pop(); a := pop(); push(math.Pow(a, b)) }},

	{"sin", func() { push(math.Sin(pop())) }},
	{"cos", func() { push(math.Cos(pop())) }},
	{"tan", func() { push(math.Tan(pop())) }},
	{"log", func() { push(math.Log10(pop())) }},
	{"ln", func() { push(math.Log(pop())) }},
	{"lg", func() { push(math.Log2(pop())) }},

	{"pi", func() { push(math.Pi) }},
	{"e", func() { push(math.E) }},

	{".", func() { fmt.Printf("%g\n", pop()) }},
	{"int", func() { fmt.Printf("%d\n", int64(pop())) }},
	{"drop", func() { pop() }},
	{"dup", func() { v := pop(); push(v); push(v) }},

	{"q", func() { os.Exit(0) }},
}

func help() {
	line := ""
	for _, cmd := range commands {
		name := " " + cmd.name
		if len(line)+len(name) > 70 {
			fmt.Println(line)
			line = ""
		}
		line += name
	}
	if line != "" {
		fmt.Println(line)
	}
}

func display() {
	if len(stack) > 0 {
		fmt.Println("")
		for _, v := range stack {
			fmt.Printf(" %g\n", v)
		}
		fmt.Println("")
	}
	fmt.Printf("? ")
}

func dispatch(w string) bool {
	if f, err := strconv.ParseFloat(w, 64); err == nil {
		push(f)
		return true
	}

	for _, cmd := range commands {
		if cmd.name == w {
			cmd.does()
			return true
		}
	}

	if w == "help" || w == "?" {
		help()
		return true
	}

	return false
}

func Main() {
	if len(os.Args) == 1 {
		display()
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			lineReader := strings.NewReader(scanner.Text())
			wordScanner := bufio.NewScanner(lineReader)
			wordScanner.Split(bufio.ScanWords)

			for wordScanner.Scan() {
				w := wordScanner.Text()
				if !dispatch(w) {
					fmt.Fprintf(os.Stderr, "%s? (type \"help\" for help)\n", w)
				}
			}
			display()
		}
	} else {
		for _, w := range os.Args[1:] {
			if !dispatch(w) {
				fmt.Fprintf(os.Stderr, "%s?\n", w)
				os.Exit(1)
			}
		}
		for _, v := range stack {
			fmt.Printf("%g\n", v)
		}
	}
}
