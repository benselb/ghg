package rand

import (
	random "crypto/rand"
	"fmt"
	"math/big"
	"os"
	"regexp"
	"strconv"
)

func usage() {
	fmt.Fprintf(os.Stderr, `Usage: ghg rand spec [spec...]
Where spec is one of:
  A dice specification (such as 3d20 or d6)
  A positive integer
  password[:N[:alphabet]]
`)
	os.Exit(1)
}

func Main() {
	if len(os.Args) == 1 {
		usage()
	}

	bigInt := big.NewInt(0)

	diceRegex := regexp.MustCompile(`^([1-9][0-9]*)?d([1-9][0-9]*)$`)
	passRegex := regexp.MustCompile(`^password(:([1-9][0-9]*)(:.+)?)?$`)

	for _, s := range os.Args[1:] {
		if _, ok := bigInt.SetString(s, 0); ok && bigInt.Sign() == 1 {
			if n, err := random.Int(random.Reader, bigInt); err == nil {
				fmt.Println(n)
			} else {
				fmt.Fprintln(os.Stderr, err)
			}
		} else if submatches := diceRegex.FindStringSubmatch(s); submatches != nil {
			var n int64
			if submatches[1] != "" {
				n, _ = strconv.ParseInt(submatches[1], 10, 64)
			} else {
				n = 1
			}
			sides, _ := strconv.ParseInt(submatches[2], 10, 64)
			total := int64(0)
			for i := int64(0); i < n; i++ {
				bigInt.SetInt64(sides)
				if roll, err := random.Int(random.Reader, bigInt); err == nil {
					total += 1 + roll.Int64()
				} else {
					fmt.Fprintln(os.Stderr, err)
				}
			}
			fmt.Println(total)
		} else if submatches := passRegex.FindStringSubmatch(s); submatches != nil {
			alphabet := "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"
			length := int64(16)
			if submatches[2] != "" {
				length, _ = strconv.ParseInt(submatches[2], 10, 64)
			}
			if submatches[3] != "" {
				alphabet = submatches[3][1:]
			}
			bigInt.SetInt64(int64(len(alphabet)))

			for i := int64(0); i < length; i++ {
				if n, err := random.Int(random.Reader, bigInt); err == nil {
					fmt.Print(alphabet[n.Int64() : n.Int64()+1])
				} else {
					fmt.Fprintln(os.Stderr, err)
				}
			}

			fmt.Print("\n")
		} else {
			usage()
		}
	}
}
